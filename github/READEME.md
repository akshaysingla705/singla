# Project Management backend #

## Requirements ##
1. Postgres Database
2. Private key (common among all services)
3. Redis Database

## Environment Variables => ##

```
//service based
export PROJECT_MANAGEMENT_PORT="8000"
export ENVIRONMENT="development"
export BUILD_IMAGE="d11111"

// postgres database
export PROJECT_MANAGEMENT_DB_HOST="localhost"
export PROJECT_MANAGEMENT_DB_PORT="5432"
export PROJECT_MANAGEMENT_DB_USER="postgres"
export PROJECT_MANAGEMENT_DB_PASS="1234"
export PROJECT_MANAGEMENT_DB_NAME="nexaops_support"

//redis Database
export PROJECT_MANAGEMENT_REDIS_DB="1"
export PROJECT_MANAGEMENT_REDIS_HOST="localhost"
export PROJECT_MANAGEMENT_REDIS_PORT="6379"
export PROJECT_MANAGEMENT_REDIS_PASS=""

//jwt Configuration
export PRIVATE_KEY="same among all the nexaops backend services"
```

## How to run the app ##

### 1. Configuration using environment variables ###

```
i.    Export above all environment variables
ii.   Build the app or binary -> command -> `$ go install`
iii.  Run the app or binary -> command -> `$GOPATH/bin/project-management --conf=environment`
```

### 2. Configuration using TOML file ###

```
i.    Create a configuration toml file by taking reference from example.toml file
ii.   Build the app or binary -> command -> `$ go install`
iii.  Run the app or binary -> command -> `$GOPATH/bin/project-management --conf=toml --file=<path of toml file`
```

`Note :- for any help regarding flags, run this command '$GOPATH/bin/project-management --help'`
